/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jdbc_zad22_dominanclass.TvSeries;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.TvSeriesMapper;

/**
 *
 * @author Grzegorz
 */
public class TvSeriesRepository implements Repository<TvSeries>
{
    private TvSeriesMapper tvSeriesMapper;

    private static final String tvSeriesTable = "CREATE TABLE TVSERIES"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "tvSeriesName VARCHAR(100),"
            + "PRIMARY KEY ( id ))";

    public TvSeriesRepository(Connection connection)
    {
        this.tvSeriesMapper = new TvSeriesMapper(connection);

        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("TVSERIES")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(tvSeriesTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }
    
    

    @Override
    public TvSeries GetById(int id)
    {
       return tvSeriesMapper.find((long) id); 
    }

    @Override
    public void add(TvSeries item)
    {
        tvSeriesMapper.add(item);
    }

    @Override
    public void update(TvSeries item)
    {
        tvSeriesMapper.update(item);
    
    }

    @Override
    public void remove(TvSeries item)
    {
        tvSeriesMapper.remove((long) item.getId());
    }
    
}
