/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jdbc_zad22_dominanclass.Director;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.DirectorMapper;

/**
 *
 * @author Grzegorz
 */
public class DirectorRepository implements Repository<Director>
{
    private DirectorMapper directorMapper;

    private static final String directorTable = "CREATE TABLE DIRECTOR"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "directorName VARCHAR(100),"
            + "directorDayOfBirth DATE,"
            + "directorBiography VARCHAR(255),"
            + "PRIMARY KEY ( id ))";

    public DirectorRepository(Connection connection)
    {
        this.directorMapper = new DirectorMapper(connection);

        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("DIRECTOR")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(directorTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Director GetById(int id)
    {
        return directorMapper.find((long)id);
    }

    @Override
    public void add(Director item)
    {
       directorMapper.add(item);
    }

    @Override
    public void update(Director item)
    {
        directorMapper.update(item);
    }

    @Override
    public void remove(Director item)
    {
        directorMapper.remove((long) item.getId());
    }
    
}
