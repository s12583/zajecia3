/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jdbc_zad22_dominanclass.Season;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.EpisodeMapper;
import jdbc_zadv22_mappers.SeasonMapper;

/**
 *
 * @author Grzegorz
 */
public class SeasonRepository implements Repository<Season>
{
    private SeasonMapper seasonMapper;


    private static final String seasonTable = "CREATE TABLE SEASON"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "seasonNumseasonNumberber INTEGER,"
            + "seasonYearOfRelease INTEGER,"
            + "idTvSeries INTEGER,"
            + "PRIMARY KEY ( id ))";




    public SeasonRepository(Connection connection)
    {
        this.seasonMapper = new SeasonMapper(connection);


        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("SEASON")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(seasonTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }






    
    @Override
    public Season GetById(int id)
    {
       return seasonMapper.find((long) id);
        
    }

    @Override
    public void add(Season item)
    {
        seasonMapper.add(item);
    }

    @Override
    public void update(Season item)
    {
      seasonMapper.update(item);
   
    }

    @Override
    public void remove(Season item)
    {
        seasonMapper.remove((long) item.getId());
    }
    
}
