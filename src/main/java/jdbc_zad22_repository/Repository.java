/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

/**
 *
 * @author Grzegorz
 * @param <T>
 */
public interface Repository <T> 
{
    T GetById(int id); 
    
    void add(T item);

    void update(T item);

    void remove(T item);
    

    
}
