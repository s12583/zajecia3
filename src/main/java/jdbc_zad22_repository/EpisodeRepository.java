/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import jdbc_zad22_dominanclass.Episode;
import jdbc_zad22_repository_interface.IRepositoryEpisode;
import jdbc_zadv22_mappers.EpisodeMapper;

/**
 *
 * @author Grzegorz
 */
public class EpisodeRepository implements IRepositoryEpisode
{
    private EpisodeMapper episodeMapper;
    private static final String episodeTable = "CREATE TABLE EPISODE"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "episodeName VARCHAR(100),"
            + "episodeReleaseDate DATE,"
            + "episodeNumber INTEGER,"
            + "episodeDuration TIME,"
            + "idSeason INTEGER,"
            + "PRIMARY KEY ( id ))";
    
    

    public EpisodeRepository(Connection connection)
    {
        this.episodeMapper = new EpisodeMapper(connection);
        
        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("EPISODE")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(episodeTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    

    @Override
    public Episode GetById(int id)
    {
      return episodeMapper.find((long) id);
      
    }

    @Override
    public void add(Episode item)
    {
        episodeMapper.add(item);
    }

    @Override
    public void update(Episode item)
    {
       episodeMapper.update(item);
    }

    @Override
    public void remove(Episode item)
    {
         episodeMapper.remove((long) item.getId());
    }

    @Override
    public List<Episode> allwithIdSeasons(int seasonID)
    {
        
        return episodeMapper.allwithIdSeasons(seasonID);
    }
    
}
