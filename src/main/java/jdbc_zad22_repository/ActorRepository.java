/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jdbc_zad22_dominanclass.Actor;
import jdbc_zad22_repository_interface.Repository;
import jdbc_zadv22_mappers.ActorMapper;

/**
 *
 * @author Grzegorz
 */
public class ActorRepository implements Repository<Actor>
{
   
    private ActorMapper actorMapper;

    private static final String actorTable = "CREATE TABLE ACTOR"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "actorName VARCHAR(100),"
            + "actorDayOfBirth DATE,"
            + "actorBiography VARCHAR(100),"
            + "idTvSeries INTEGER,"
            + "PRIMARY KEY ( id ))";

    public ActorRepository(Connection connection)
    {
      
        this.actorMapper = new ActorMapper(connection);

        try {

            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists = false;
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase("ACTOR")) {
                    tableExists = true;
                    break;
                }
            }
            if (!tableExists) {
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(actorTable);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Actor GetById(int id)
    {
        return actorMapper.find((long) id);
    }

    @Override
    public void add(Actor item)
    {
        actorMapper.add(item);
    }

    @Override
    public void update(Actor item)
    {
        actorMapper.update(item);
    }

    @Override
    public void remove(Actor item)
    {
        actorMapper.remove((long)item.getId());
    }
    
    

    
}
