package jdbc_zadv22;


import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jdbc_zad22_db_meneger.ConnectionDB;
import jdbc_zad22_db_meneger.CreateTable;
import jdbc_zad22_dominanclass.Actor;
import jdbc_zad22_dominanclass.Director;
import jdbc_zad22_dominanclass.Episode;
import jdbc_zad22_dominanclass.TvSeries;
import jdbc_zad22_repository.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws SQLException {
        ConnectionDB con = new ConnectionDB();
        CreateTable tab = new CreateTable(con.getCon());
        //tab.setTvSeriesTable();

        ActorRepository rep = new ActorRepository(con.getCon());

        Date dat1 = new Date(19 - 07 - 1994);

        Actor act1 = new Actor();
        act1.setId_TvSeries(2);
        act1.setDateofBirth(dat1);
        act1.setBiography("asdasdasd");
        act1.setId(1);
        act1.setName("pirewszyactor");

        Actor act2 = new Actor();
        act2.setId_TvSeries(2);
        act2.setDateofBirth(dat1);
        act2.setBiography("Grzegorz");
        //act1.setId(1);
        act2.setName("nolan");

        //rep.add(act2);

        //Actor act3 = rep.GetById(2);

        //System.out.print(act3.toString());
        //-------dIRECTO --

        DirectorRepository rep_dir = new DirectorRepository(con.getCon());

        Director dir1 = new Director();
        dir1.setBiography("pras sa a");
        dir1.setDateofBirth(dat1);
        dir1.setName("vega");

        rep_dir.add(dir1);

        Director dir2 = rep_dir.GetById(1);

        System.out.print(dir2.toString());

        TvSeries tv1 = new TvSeries();
        tv1.setName("Mr roobot");

        TvSeriesRepository rep_tv = new TvSeriesRepository(con.getCon());

        rep_tv.add(tv1);

        System.out.print(rep_tv.GetById(1).toString());
        
        Episode ep1 = new Episode();
        ep1.setId_Season(1);
        ep1.setName("pierwszyepizot");
        
        Duration oneHours = Duration.ofHours(1);;
        ep1.setDuration(oneHours);
        ep1.setRealaseDate(dat1);
        Episode ep2 = new Episode();
        ep2.setId_Season(1);
        ep2.setName("drugiepizot");
        ep2.setDuration(oneHours);
        ep2.setRealaseDate(dat1);
        Episode ep3 = new Episode();
        ep3.setId_Season(1);
        ep3.setName("tzreciepizot");
        ep3.setDuration(oneHours);
        ep3.setRealaseDate(dat1);
        
        EpisodeRepository rep_ep = new EpisodeRepository(con.getCon());
        
        rep_ep.add(ep1);
        rep_ep.add(ep2);
        rep_ep.add(ep3);
        
        List <Episode> lepisode = new ArrayList();
        
        lepisode = rep_ep.allwithIdSeasons(1);
        
        System.out.print(lepisode.toString());

        SeasonRepository rep1_ep = new SeasonRepository(con.getCon());
        
       

    }
}
