/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_repository_interface;

import java.util.List;
import jdbc_zad22_dominanclass.Episode;

/**
 *
 * @author Grzegorz
 */
public interface IRepositoryEpisode extends Repository<Episode>
{
    List<Episode> allwithIdSeasons (int seasonID);
}
