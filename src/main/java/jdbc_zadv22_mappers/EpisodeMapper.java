/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import jdbc_zad22_dominanclass.Episode;

/**
 *
 * @author Grzegorz
 */
public class EpisodeMapper extends AbstractMapper <Episode> 
{
    private static final String COLUMNS = "id, episodeName,episodeReleaseDate,episodeNumber,episodeDuration";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM Episode WHERE id=?";
    public static final String INSERT_STM = "INSERT INTO Episode(episodeName,episodeReleaseDate,episodeNumber,episodeDuration,idSeason) VALUES(?,?,?,?,?)";
    public static final String UPDATE_STM = "UPDATE Episode SET(episodeName,episodeReleaseDate,episodeNumber,episodeDuration)=(?,?,?,?) WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM Episode WHERE id=?";
    public static final String SELECT_BY_IDSEASON = "SELECT " + COLUMNS + " FROM Episode WHERE idSeason=?";


    public EpisodeMapper(Connection connection)
    {
        super(connection);
    }

    public List<Episode> allwithIdSeasons (int seasonID)
    {

        List<Episode> result = new ArrayList<>();

        PreparedStatement selectBySeasonID;
        try {
            selectBySeasonID = connection.prepareStatement(SELECT_BY_IDSEASON );

            selectBySeasonID.setInt(1, seasonID);

            ResultSet rs = selectBySeasonID.executeQuery();
            while (rs.next()) {
                Episode episode1 = doLoad(rs);
                result.add(episode1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected String findStatement()
    {
       return FIND_STM;
    }

    @Override
    protected String insertStatement()
    {
        return INSERT_STM;
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM;
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Episode doLoad(ResultSet rs) throws SQLException
    {
      java.sql.Time sqlTime = rs.getTime("episodeDuration");
      LocalTime localTime = sqlTime.toLocalTime();      
      
              
        Episode episode_tmp = new Episode();
        episode_tmp.setId(rs.getInt("id"));
        episode_tmp.setName(rs.getString("episodeName"));
        episode_tmp.setDuration( Duration.between(LocalTime.MIDNIGHT, localTime));
        episode_tmp.setRealaseDate(rs.getDate("episodeReleaseDate"));
        episode_tmp.setEpisodeNumber(rs.getInt("episodeNumber"));
        return episode_tmp;  
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Episode entity) throws SQLException
    {
        LocalTime localTime = LocalTime.MIDNIGHT.plus(entity.getDuration());
        
        
        statement.setString(1,entity.getName());
        statement.setDate(2,new java.sql.Date(entity.getRealaseDate().getTime()));
        statement.setInt(3,entity.getEpisodeNumber());
        statement.setTime(4,java.sql.Time.valueOf(localTime));
        statement.setInt(5,entity.getId_Season());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Episode entity) throws SQLException
    {
        parametrizeInsertStatement(statement, entity);
        statement.setLong(1,entity.getId());
    }
    
}
