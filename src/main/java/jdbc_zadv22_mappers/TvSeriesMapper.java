/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc_zad22_dominanclass.TvSeries;

/**
 *
 * @author Grzegorz
 */
public class TvSeriesMapper extends AbstractMapper <TvSeries>
{
    private static final String COLUMNS = "id, tvSeriesName";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM TvSeries WHERE id=?";
    public static final String INSERT_STM = "INSERT INTO TvSeries(tvSeriesName) VALUES(?)";
    public static final String UPDATE_STM = "UPDATE TvSeries SET(tvSeriesName)=(?) WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM TvSeries WHERE id=?";

    public TvSeriesMapper(Connection connection)
    {
        super(connection);
    }

    @Override
    protected String findStatement()
    {
        return FIND_STM;
    }

    @Override
    protected String insertStatement()
    {
        return INSERT_STM;
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM;
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected TvSeries doLoad(ResultSet rs) throws SQLException
    {
        TvSeries tvseries_tmp = new TvSeries();
        tvseries_tmp.setId(rs.getInt("id"));
        tvseries_tmp.setName(rs.getString("tvSeriesName"));
        return tvseries_tmp ;
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, TvSeries entity) throws SQLException
    {
        statement.setString(1,entity.getName());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, TvSeries entity) throws SQLException
    {
        parametrizeInsertStatement(statement, entity);
        statement.setLong(1,entity.getId());
        
    }
    
}
