/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc_zad22_dominanclass.Season;

/**
 *
 * @author Grzegorz
 */
public class SeasonMapper extends AbstractMapper<Season>
{
   private static final String COLUMNS = "id, seasonNumseasonNumberber,seasonYearOfRelease";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM Season WHERE id=?";
    public static final String INSERT_STM = "INSERT INTO Season(seasonNumber,seasonYearOfRelease,idTvSeries) VALUES(?,?,?)";
    public static final String UPDATE_STM = "UPDATE Season SET(seasonNumber,seasonYearOfRelease,idTvSeries)=(?,?,?) WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM Season WHERE id=?";
       
    public SeasonMapper(Connection connection)
    {
        super(connection);
    }

    @Override
    protected String findStatement()
    {
        return FIND_STM; 
    }

    @Override
    protected String insertStatement()
    {
        return INSERT_STM; 
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM;
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Season doLoad(ResultSet rs) throws SQLException
    {
        Season season_tmp = new Season();
        season_tmp.setId(rs.getInt("id"));
        season_tmp.setSeasonNumber(rs.getInt("seasonNumber"));
        season_tmp.setYearOfRealease(rs.getInt("seasonYearOfRelease"));
        
        return season_tmp; 
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Season entity) throws SQLException
    {
        statement.setInt(1,entity.getSeasonNumber());
        statement.setInt(2,entity.getYearOfRealease());
        statement.setInt(3,entity.getIdTvSeries());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Season entity) throws SQLException
    {
        parametrizeInsertStatement(statement,entity);
        statement.setLong(1,entity.getId());
    }
    
}
