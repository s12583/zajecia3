/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc_zad22_dominanclass.Actor;

/**
 *
 * @author Grzegorz
 */
public class ActorMapper extends AbstractMapper<Actor>
{
    private static final String COLUMNS = "id, actorName,actorDayOfBirth,actorBiography";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM Actor WHERE id=?";
    public static final String INSERT_STM = "INSERT INTO Actor(actorName,actorDayOfBirth,actorBiography,idTvSeries) VALUES(?,?,?,?)";
    public static final String UPDATE_STM = "UPDATE Actor SET(actorName,actorDayOfBirth,actorBiography,idTvSeries)=(?,?,?,?) WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM Actor WHERE id=?";

    public ActorMapper(Connection connection)
    {
        super(connection);
    }

    @Override
    protected String findStatement()
    {
        return FIND_STM;
    }

   

    @Override
    protected String insertStatement()
    {
        return INSERT_STM ; 
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM ; 
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Actor doLoad(ResultSet rs) throws SQLException
    {
        Actor actor_temp = new Actor();
        actor_temp.setId(rs.getInt("id"));
        actor_temp.setName(rs.getString("actorName"));
        actor_temp.setDateofBirth(rs.getDate("actorDayOfBirth"));
        actor_temp.setBiography(rs.getString("actorBiography"));
        return actor_temp;
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Actor entity) throws SQLException
    {
         statement.setString(1,entity.getName());
        statement.setDate(2,new java.sql.Date(entity.getDateofBirth().getTime()));
        statement.setString(3,entity.getBiography());
        statement.setInt(4,entity.getId_TvSeries());
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Actor entity) throws SQLException
    {
        parametrizeInsertStatement(statement, entity);
        statement.setLong(1,entity.getId());
    }

   
    
}
