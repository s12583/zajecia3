/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zadv22_mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc_zad22_dominanclass.Director;

/**
 *
 * @author Grzegorz
 */
public class DirectorMapper extends AbstractMapper<Director> 
{
    private static final String COLUMNS = "id, directorName,directorDayOfBirth,directorBiography";
    public static final String FIND_STM = "SELECT " + COLUMNS + " FROM Director WHERE id=?";
    public static final String INSERT_STM = "INSERT INTO Director(directorName,directorDayOfBirth,directorBiography) VALUES(?,?,?)";
    public static final String UPDATE_STM = "UPDATE Director SET(directorName,directorDayOfBirth,directorBiography)=(?,?,?) WHERE id=?";
    public static final String DELETE_STM = "DELETE FROM Director WHERE id=?";
    
    public DirectorMapper(Connection connection)
    {
        super(connection);
    }

    @Override
    protected String findStatement()
    {
        return FIND_STM;
    }

   

    @Override
    protected String insertStatement()
    {
        return INSERT_STM ; 
    }

    @Override
    protected String updateStatement()
    {
        return UPDATE_STM ; 
    }

    @Override
    protected String removeStatement()
    {
        return DELETE_STM;
    }

    @Override
    protected Director doLoad(ResultSet rs) throws SQLException
    {
        Director director_tmp = new Director();
        director_tmp.setId(rs.getInt("id"));
        director_tmp.setName(rs.getString("directorName"));
        director_tmp.setDateofBirth(rs.getDate("directorDayOfBirth"));
        director_tmp.setBiography(rs.getString("directorBiography"));
       
        return director_tmp;
    }

    @Override
    protected void parametrizeInsertStatement(PreparedStatement statement, Director entity) throws SQLException
    {
        statement.setString(1,entity.getName());
        statement.setDate(2,new java.sql.Date(entity.getDateofBirth().getTime()));
        statement.setString(3,entity.getBiography());
        
    }

    @Override
    protected void parametrizeUpdateStatement(PreparedStatement statement, Director entity) throws SQLException
    {
        parametrizeInsertStatement(statement, entity);
        statement.setLong(1,entity.getId());
    }
    
}
