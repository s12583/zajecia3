/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc_zad22_db_meneger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;


public class CreateTable
{

    private Statement stmt;
    
    private static final String tvSeriesTable = "CREATE TABLE TVSERIES"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "tvSeriesName VARCHAR(100),"
             + "PRIMARY KEY ( id ))";

    private static final String directorTable = "CREATE TABLE DIRECTOR"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "directorName VARCHAR(100),"
            + "directorDayOfBirth DATE,"
            + "directorBiography VARCHAR(255),"
            + "PRIMARY KEY ( id ))";

    private static final String actorTable = "CREATE TABLE ACTOR"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "actorName VARCHAR(100),"
            + "actorDayOfBirth DATE,"
            + "actorBiography VARCHAR(100),"
            + "idTvSeries INTEGER,"
            + "PRIMARY KEY ( id ))";

    private static final String seasonTable = "CREATE TABLE SEASON"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "seasonNumseasonNumberber INTEGER,"
            + "seasonYearOfRelease INTEGER,"
            + "idTvSeries INTEGER,"
            + "PRIMARY KEY ( id ))";

    private static final String episodeTable = "CREATE TABLE EPISODE"
            + "(id INTEGER NOT NULL AUTO_INCREMENT,"
            + "episodeName VARCHAR(100),"
            + "episodeReleaseDate DATE,"
            + "episodeNumber INTEGER,"
            + "episodeDuration TIME,"
            + "idSeason INTEGER,"
            + "PRIMARY KEY ( id ))";

    public CreateTable(Connection con) throws SQLException
    {
        try
        {
            this.stmt = con.createStatement();
         

        } catch (SQLException e)
        {
            System.out.println("Brak polaczenia");
            e.printStackTrace(System.out);
        }

    }

    public void setTvSeriesTable() throws SQLException
    {

        try
        {



           
            stmt.executeUpdate(tvSeriesTable);
            
            stmt.executeUpdate(directorTable);
            stmt.executeUpdate(actorTable);
            stmt.executeUpdate(seasonTable);
            stmt.executeUpdate(episodeTable);

        } catch (SQLException e)
        {
            e.printStackTrace(System.out);
            
        }
        System.out.println("Tabela utworzona pomyślnie");
    }
    
    

}
